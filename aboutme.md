---
layout: page
title: About me
subtitle: What am I interested in
---

Hello, I'm Joseph McKinsey. Things I'm interested include (sorted in terms of current familiarity not interest):

- Functional Programming
  - Haskell
  - Racket (dialect of lisp)
- Algorithms
- Numerical Approximation
- Numerical Partial Differential Equations
- Number Theory
- Utility functions
- Interval Arithmetic
- Linear Algebra
- Game Theory
- Computational Geometry
- Stochastic Modeling
- Scientific Visualization
- Type theory
- Bayesian Statistics
- Category Theory
- Macroeconomics
- Fourier Analysis
- Information Theory
- Perturbation Theory
- Scala
- Erlang
- Elm

### Other Links

#### [Resume](https://github.com/josephmckinsey/resume/blob/master/resume.pdf)

#### [LinkedIn](https://www.linkedin.com/in/joseph-mckinsey-356195146/)

